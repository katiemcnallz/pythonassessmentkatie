import re
import shutil

fh=open("hosts.real","r")
ptn2=re.compile("#.*")
ptn=re.compile("([a-fA-F0-9]{2}[:|\-]?){6}") #MAC
ptn5=re.compile("\s+")
outpeopleFH=open("tempfile","w") #created phones.txt
ipaddress=open("ipaddress","w")


#a. Remove all of the comments from the file

for line in fh:
    #remove lines that beign with a #
    if re.search('Regression: ',line):
        line=ptn2.sub("", line)

    if re.search('^#',line):
        continue

    if re.search('#',line):
        line=ptn2.sub("", line)

    #removing spaces
    data=ptn5.split(line)
    outpeopleFH.write(" ".join(data)+"\n") #added the data from fh into tempfile
    
    #grabbing ipaddresses and writing it to a new file
    ipaddress.write(data[0]+"\n")


#b. Extract all of the MAC addresses from the file
    phonenum=ptn.search(line)
    if phonenum:
        print(phonenum.group().replace(",",""))


fh.close()
outpeopleFH.close()

shutil.move('tempfile', 'hosts.real') #moving file back to original
