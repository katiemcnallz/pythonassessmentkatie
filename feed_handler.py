import re


handler=open("opra_example_regression.log","r")

tp=tt=tv=rp=""
ptn2=re.compile("Regression:")
ptn=re.compile("\s+")


for i in handler:


    if re.search("^Regression:&",i):
        continue

    if re.search("Regression:",i):
        i=ptn2.sub("", i)

    if re.search('Record Publish:',i):
        rp=i


    if re.search('Type: Trade',i):
        tt=i


    if rp and tt:
        if re.search('wTradeVolume',i):
            tv=i


        if re.search('wTradePrice',i):
            tp=i

    if rp and tt and tv and tp:
        print(rp.rstrip("\r\n")+tt.rstrip("\r\n"))
        print(tp+tv.rstrip("\r\n"))
        tp=tt=tv=rp=""

##Get rid of regression
